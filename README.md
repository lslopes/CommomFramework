# CommomFramework

[![CI Status](https://img.shields.io/travis/Leandro Lopes/CommomFramework.svg?style=flat)](https://travis-ci.org/Leandro Lopes/CommomFramework)
[![Version](https://img.shields.io/cocoapods/v/CommomFramework.svg?style=flat)](https://cocoapods.org/pods/CommomFramework)
[![License](https://img.shields.io/cocoapods/l/CommomFramework.svg?style=flat)](https://cocoapods.org/pods/CommomFramework)
[![Platform](https://img.shields.io/cocoapods/p/CommomFramework.svg?style=flat)](https://cocoapods.org/pods/CommomFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CommomFramework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CommomFramework'
```

## Author

Leandro Lopes, leandro.lopes@tfhstudios.com

## License

CommomFramework is available under the MIT license. See the LICENSE file for more info.
